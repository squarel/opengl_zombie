#include "weapon.h"
#include "vec3f.h"
#include "movable.h"
#include "draw_helper.h"

#include "opengl.h"

void
Bullet::draw()
{
    glPushMatrix();
    glTranslatef(pos[0], pos[1], pos[2] + r);
    glColor3f(color[0], color[1], color[2]);
    glutSolidSphere(r, 12, 12);
    glPopMatrix();
}

Bullet::Bullet(Vec3f _pos, float _velocity, Vec3f _color, float _radius): Movable(_pos, dir::right, _velocity, _color)
{
    r = _radius;
}
