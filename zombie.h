#ifndef GUARD_ZOMBIE_H
#define GUARD_ZOMBIE_H

#include "vec3f.h"
#include "movable.h"

class Zombie: public Movable
{
    public:
        //position, velocity, color
        Zombie(Vec3f, float, Vec3f);
        void draw();
};

#endif
