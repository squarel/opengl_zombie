#include <ctime>
#include <cstdlib>
#include <vector>
#include <string>

#include "vec3f.h"
#include "opengl.h"
#include "player.h"
#include "draw_helper.h"
#include "movable.h"
#include "zombie.h"
#include "text3d.h"


// Def
typedef std::vector<Movable*> MovableList;

// Constants
const int refresh_rate = 25;
const float max_x = 3.0;
const float max_y = 1.0;

// Enum
enum class GameStatus {PLAYING, PAUSE, FINISH, DEMO};

// Declare initial variables
static float _angle = 0;
static Player *pl;
static MovableList bullet_list;
static MovableList zombie_list;
static GameStatus game_status;
static int score = 0;
static int life = 5;

void
initRendering()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_NORMALIZE);
    glEnable(GL_SMOOTH);

    t3dInit();
}

void
handleResize(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);
}

void
drawScore(Vec3f pos)
{
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    std::string str = "Score: " + std::to_string(score) + "  Life: " + std::to_string(life);
    glPushMatrix();
    glTranslatef(pos[0], pos[1], pos[2]);
    glScalef(0.2f, 0.2f, 0.2f);
    glColor3f(1.0f, 1.0f, 1.0f);
    t3dDraw2D(str, 0, 0);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void
drawMenu()
{
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    std::string str = "Press 'd' for demo\nPress 'p' to play or pause\n'j, k, SPACE' as move and shoot\nESC' to exit\n";
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.0f);
    glScalef(0.3f, 0.3f, 0.3f);
    glColor3f(1.0f, 1.0f, 1.0f);
    t3dDraw2D(str, 0, 0);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void
drawBigText(std::string str)
{
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.0f);
    glScalef(0.5f, 0.5f, 0.5f);
    glColor3f(1.0f, 1.0f, 1.0f);
    t3dDraw2D(str, 0, 0);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void
drawScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(0.0f, 0.0f, -5.0f);

    //draw score, life
    Vec3f left_top(-3.0f, 1.5f, 0.0f);
    drawScore(left_top);

    if(game_status == GameStatus::PAUSE)
    {
        drawMenu();
    }
    else if(game_status == GameStatus::DEMO)
    {
        std::string demo = "DEMO";
        drawBigText(demo);
    }
    else if(game_status == GameStatus::FINISH)
    {
        std::string demo = "Sorry, you're dead.";
        drawBigText(demo);
    }

    glRotatef(-_angle, 1.0f, 1.0f, 0.0f);

    GLfloat ambientLight[] = {0.5f, 0.5f, 0.5f, 1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

    GLfloat lightColor[] = {0.5f, 0.5f, 0.5f, 1.0f};
    GLfloat lightPos[] = {-0.2f, 0.3f, -1, 0.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
    glLightfv(GL_LIGHT0, GL_PROJECTION, lightPos);

    //draw the floor
    glPushMatrix();
    glBegin(GL_QUADS);

    glColor3f(0.0f, 0.75f, 0.0f);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(-max_x, -max_y, 0.0f);
    glVertex3f(-max_x, max_y, 0.0f);
    glVertex3f(max_x, max_y, 0.0f);
    glVertex3f(max_x, -max_y, 0.0f);

    glEnd();
    glPopMatrix();


    //draw player
    pl->draw();

    //draw bullets
    for(MovableList::iterator i= bullet_list.begin(); i != bullet_list.end(); ++i)
        (*i)->draw();

    //draw zombies
    for(MovableList::iterator i= zombie_list.begin(); i != zombie_list.end(); ++i)
        (*i)->draw();


    glutSwapBuffers();
}

float gen_random(float hi, float low)
{
    return low + static_cast<float> (rand()) / static_cast<float> (RAND_MAX/(hi - low));
}


// create "zombie" at random position with random speed
Zombie*
create_zombie()
{
    float y = gen_random(1.0f, -1.0f);
    Vec3f pos(max_x, y, 0.0f);
    Vec3f color(1.0f, 0.0f, 0.75f);
    float speed = gen_random(0.05f, 0.01f);
    Zombie *z = new Zombie(pos, speed, color);
    return z;
}

void
collision_detection()
{
    Vec3f bullet_pos;
    bool collision_flag;
    for(MovableList::iterator bullet_i = bullet_list.begin(); bullet_i != bullet_list.end();)
    {
        collision_flag = false;
        bullet_pos = (*bullet_i)->get_pos();

        // check bullet wall collision
        if(bullet_pos[0] > max_x)
        {
            bullet_i = bullet_list.erase(bullet_i);
            continue;
        }

        for(MovableList::iterator zombie_i = zombie_list.begin(); zombie_i != zombie_list.end();)
        {
            if(collide(**zombie_i, **bullet_i))
            {
                zombie_i = zombie_list.erase(zombie_i);
                collision_flag = true;
                score++;
                break;
            }
            else
            {
                ++zombie_i;
            }
        }

        if(collision_flag)
            bullet_i = bullet_list.erase(bullet_i);
        else
            ++bullet_i;
    }

    Vec3f zombie_pos;
    for(MovableList::iterator zombie_i = zombie_list.begin(); zombie_i != zombie_list.end();)
    {
        zombie_pos = (*zombie_i)->get_pos();
        if(zombie_pos[0] <= -max_x)
        {
            zombie_i = zombie_list.erase(zombie_i);
            life--;
            if(life == 0)
                game_status = GameStatus::FINISH;
        }
        else
            ++zombie_i;
    }

    return;
}


void
advance()
{
    static int counter = 0;
    if(counter >= 100)
    {
        counter = 0;
        Zombie* new_zombie = create_zombie();
        zombie_list.push_back(new_zombie);
    }
    else
        counter++;

    if(game_status == GameStatus::DEMO && counter % 8 == 0)
    {
        Vec3f player_pos = pl->get_pos();
        static bool up = true;
        if(up)
            pl->move_up();
        else
            pl->move_down();
        Bullet *b = pl->fire();
        bullet_list.push_back(b);

        if(player_pos[1] >= (max_y - 0.1))
            up = false;
        else if(player_pos[1] <= (-max_y - 0.1))
            up = true;
    }

    for(MovableList::iterator i = bullet_list.begin(); i != bullet_list.end(); ++i)
        (*i)->move();
    for(MovableList::iterator i = zombie_list.begin(); i != zombie_list.end(); ++i)
        (*i)->move();

    collision_detection();
    return;
}

void
update(int value)
{
    _angle += 1.0f;
    if (_angle > 90)
    {
        _angle -= 180;
    }
    if(game_status == GameStatus::DEMO || game_status == GameStatus::PLAYING)
        advance();
    glutPostRedisplay();
    glutTimerFunc(refresh_rate, update, 0);
}

void
cleanup()
{
    return;
}

void
pause()
{
    game_status = GameStatus::PAUSE;
}

void
handleKeypress(unsigned char key, int x, int y)
{
    Vec3f player_pos;
    switch(key)
    {
        case 27:
            cleanup();
            exit(0);
        case 'k':
            if(game_status == GameStatus::PLAYING)
            {
                player_pos = pl->get_pos();
                if(player_pos[1] < max_y)
                    pl->move_up();
            }
            break;
        case 'j':
            if(game_status == GameStatus::PLAYING)
            {
                player_pos = pl->get_pos();
                if(player_pos[1] >= -max_y)
                    pl->move_down();
            }
            break;
        case 'd':
            game_status = GameStatus::DEMO;
            break;
        case 'p':
            if(game_status != GameStatus::PAUSE)
            {
                if(game_status == GameStatus::FINISH)
                {
                    score = 0;
                    life = 5;
                }
                pause();
            }
            else
                game_status = GameStatus::PLAYING;
            break;
        case ' ':
            if(game_status == GameStatus::PLAYING)
            {
                Bullet *b = pl->fire();
                bullet_list.push_back(b);
            }
            break;
    }
}

int
main(int argc, char** argv)
{
    srand((unsigned int)time(0));

    Vec3f pos(-max_x, 0.0f, 0.0f);
    pl = new Player(pos, 0.1f);

    game_status = GameStatus::PAUSE;


    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 400);

    glutCreateWindow("World War Z");
    initRendering();

    glutDisplayFunc(drawScene);
    glutKeyboardFunc(handleKeypress);
    glutReshapeFunc(handleResize);
    glutTimerFunc(refresh_rate, update, 0);

    glutMainLoop();
    return 0;
}
