#include "draw_helper.h"
#include "opengl.h"

void
draw_cube(float _longX, float _widthY, float _highZ)
{
    float x, y, z;
    x = _longX;
    y = _widthY;
    z = _highZ;
    // front
    glVertex3f(x, 0.0f, 0.0f);
    glVertex3f(x, 0.0f, 0.0f);
    glVertex3f(x, y, 0.0f);
    glVertex3f(0.0f, y, 0.0f);
    // back
    glVertex3f(0.0f, 0.0f, -z);
    glVertex3f(x, 0.0f, -z);
    glVertex3f(x, y, -z);
    glVertex3f(0.0f, y, -z);
    // right
    glVertex3f(x, 0.0f, 0.0f);
    glVertex3f(x, 0.0f, -z);
    glVertex3f(x, y, -z);
    glVertex3f(x, y, 0.0f);
    // left
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, -z);
    glVertex3f(0.0f, y, -z);
    glVertex3f(0.0f, y, 0.0f);
    // top
    glVertex3f(0.0f, y, 0.0f);
    glVertex3f(x, y, 0.0f);
    glVertex3f(x, y, -z);
    glVertex3f(0.0f, y, -z);
    // bottom
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(x, 0.0f, 0.0f);
    glVertex3f(x, 0.0f, -z);
    glVertex3f(0.0f, 0.0f, -z);
}
