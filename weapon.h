#ifndef GUARD_WEAPON_H
#define GUARD_WEAPON_H

#include "vec3f.h"
#include "movable.h"

class Bullet: public Movable
{
    private:
        float r; // Radius

    public:
        // position, velocity, color, radius
        Bullet(Vec3f, float, Vec3f, float);
        void draw();
};

#endif
