#include "zombie.h"
#include "movable.h"
#include "vec3f.h"
#include "opengl.h"
#include "draw_helper.h"

Zombie::Zombie(Vec3f _pos, float _velocity, Vec3f _color): Movable(_pos, dir::left, _velocity, _color)
{

}

void
Zombie::draw()
{
    glPushMatrix();
    glTranslatef(pos[0], pos[1], pos[2]);

    glBegin(GL_QUADS);

    glColor3f(color[0], color[1], color[2]);
    draw_cube(0.1f, 0.1f, -0.1f);

    glEnd();
    glPopMatrix();

}
