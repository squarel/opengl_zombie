CC = g++
CFLAGS = -Wall -Wno-deprecated-declarations -std=c++11
PROG = a.out

SRCS = main.cpp imageloader.cpp md2model.cpp text3d.cpp vec3f.cpp draw_helper.cpp player.cpp weapon.cpp movable.cpp zombie.cpp

ifeq ($(shell uname),Darwin)
	LIBS = -framework OpenGL -framework GLUT
else
	LIBS = -lglut
endif

all: $(PROG)

$(PROG):	$(SRCS)
	$(CC) $(CFLAGS) -o $(PROG) $(SRCS) $(LIBS)

clean:
	rm -f $(PROG)
