#ifndef GUARD_DRAW_HELPER_H
#define GUARD_DRAW_HELPER_H

#include "opengl.h"

void
draw_cube(float _longX, float _widthY, float _highZ);

#endif
