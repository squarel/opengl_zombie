#include "movable.h"
#include <vector>
#include <cmath>

Movable::Movable(Vec3f _pos, Direction _dir, float _velocity, Vec3f _color)
{
    pos = _pos;
    dir = _dir;
    velocity = _velocity;
    color = _color;
}

Vec3f Movable::get_pos()
{
    return pos;
}

Movable::~Movable()
{

}

void
Movable::move()
{
    pos += dir * velocity;
}


bool collide(Movable& one, Movable& other)
{
    if(fabs(one.pos[0] - other.pos[0]) < 0.1 &&
            fabs(one.pos[1] - other.pos[1]) < 0.1)
    {
        //printf("collide, x:%f, y:%f\n", fabs(one.pos[0] - other.pos[0]), fabs(one.pos[1] - one.pos[1]));
        return true;
    }
    return false;
}
