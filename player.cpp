#include "player.h"
#include "opengl.h"
#include "draw_helper.h"
#include "weapon.h"

Player::Player(Vec3f _pos, float _step)
{
    pos = _pos;
    step = _step;
}

void
Player::draw()
{
    glPushMatrix();
    glTranslatef(pos[0], pos[1], pos[2]);

    glBegin(GL_QUADS);

    glColor3f(0.75f, 0.0f, 0.0f);
    draw_cube(0.1f, 0.1f, -0.1f);

    glEnd();
    glPopMatrix();
}

Vec3f
Player::get_pos()
{
    return pos;
}

void
Player::move_up()
{
    Vec3f up(0.0f, 1.0f, 0.0f);
    pos += up * step;
    //printf("ppos:%f, %f, %f\n", pos[0], pos[1], pos[2]);
}

void
Player::move_down()
{
    Vec3f down(0.0f, -1.0f, 0.0f);
    pos += down * step;
    //printf("ppos:%f, %f, %f\n", pos[0], pos[1], pos[2]);
}

Bullet*
Player::fire()
{
    Vec3f right(1.0f, 0.0f, 0.0f);
    Vec3f blue(0.0f, 0.0f, 0.75f);
    Bullet *b = new Bullet(pos, 0.2f, blue, 0.05);
    printf("pos:%f, %f, %f\n", pos[0], pos[1], pos[2]);
    return b;
}
