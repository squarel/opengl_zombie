#ifndef GUARD_MOVABLE_H
#define GUARD_MOVABLE_H

#include "vec3f.h"
#include <vector>

typedef Vec3f Direction;
namespace dir {
    const Direction left(-1.0f, 0.0f, 0.0f);
    const Direction right(1.0f, 0.0f, 0.0f);
    const Direction up(0.0f, 1.0f, 0.0f);
    const Direction down(0.0f, -1.0f, 0.0f);
}

class Movable
{
    protected:
        Vec3f pos;
        Direction dir;
        float velocity;
        Vec3f color;

    public:
        Movable(Vec3f, Direction, float, Vec3f);
        ~Movable();
        Vec3f get_pos();
        friend bool collide(Movable&, Movable&);
        void move();
        virtual void draw() {};

};

#endif
