#ifndef GUARD_PLAYER_H
#define GUARD_PLAYER_H

#include "weapon.h"
#include "vec3f.h"

class Player
{
    private:
        Vec3f pos;
        float step;
        //Weapon weapon;

    public:
        Player(Vec3f, float);
        void draw();
        void move_up();
        void move_down();
        Vec3f get_pos();
        Bullet* fire();
};

#endif
